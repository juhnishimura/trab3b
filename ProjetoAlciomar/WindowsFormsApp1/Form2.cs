﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Mysql;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            carregarComboBox();
            carregarDadosGrid();

            //configaração incial para adequar foto na exibição
            pbFotoAluno.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void carregarComboBox()
        {
            using (produtoContext db = new produtoContext())
            {
                //Adicionando programaticamente um item na comboBox para poder iniciar corretamente o programa
                var list = new List<Object>();

                list.Add(new { Id = -1, Nome = "Digite o nome" });
                list.AddRange(db.Fornecedor.Select(x => new { x.Id, x.Nome }).ToList());

                //carregando a cmbox1
                cmbSala.DataSource = list;
                cmbSala.DisplayMember = "Nome";
                cmbSala.ValueMember = "Id";
            };
        }

       private void carregarDadosGrid()
        {

            using ( var db = new produtoContext())
            {
                dgvAlunos.DataSource = db.Produto.Select(x =>
                    new
                    {
                        cod_produto = x.id,
                        descricao = x.descricao,
                        unidade = x.unidade,
                        fornecedor = x.IdfornecedorNavigation.Nome,
                       marca= x.marca
                        modelo = x.modelo
                         Garantia = x.Garantia
                    }).ToList();

                //Configuracoes de DataGridView
                dgvAlunos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgvAlunos.AutoGenerateColumns = false;
                dgvAlunos.ReadOnly = true;


                //Customizando as colunas
                dgvAlunos.Columns["Id"].Visible = false;
                //dgvAluno.Columns["Nome"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //dgvAluno.Columns["Idade"].HeaderText = "Idade do aluno";
                dgvAlunos.Columns["Idade"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dgvAlunos.Columns["Sala"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dgvAlunos.Columns["DataMatricula"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            }

        }
        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using ( var db = new produtoContext())
            {
                produto estudante = new produto();
            
                estudante.descricao = txt1.Text;
                estudante.unidade = Convert.ToInt16(txt2.Text);
                estudante.marca = txt5.Text;
                estudante.modelo = txt3.Text;
                estudante.Garantia = txt4.Text;
                estudante.IdfornecedorNavigation = Convert.ToInt32(cmbSala.SelectedValue);
               

               
                
                    db.Produto.Add(estudante);


                db.SaveChanges();

                MessageBox.Show("Estudante Adicionado!");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Selecione a foto do Estudnate";
                dlg.Filter = "All Files|*.*|JPG|*.jpg|PNG|*.png|GIF|*.gif";
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property

                    pbFotoAluno.ImageLocation = dlg.FileName;
                }
            }
            }


        

        public Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            {
                // retira todo texto que estão nos text box
                foreach (Control c in Controls)
                {
                    if (c is TextBox)
                    {
                        c.Text = "";
                    }
                }

                //comboBox
                cmbSala.SelectedIndex = 0;

                //button
                button2.Text = "Cadastrar";
                button4.Enabled = false;

                //Imagem
                pbFotoAluno.Image = null;
                //Data da Matricula
              
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            {
                var _grid = dgEstudante.CurrentRow;

                string nomeAluno = _grid.Cells["Nome"].Value.ToString();

                DialogResult confirm = MessageBox.Show("Deseja Continuar?", "Deletar Aluno - " + nomeAluno, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                if (DialogResult.Yes == confirm)
                {
                    using (var db = new produtoContext())
                    {
                        int _id = Convert.ToInt16(_grid.Cells["Id"].Value);
                        Estudante aluno = db.Estudante.FirstOrDefault(x => x.Id == _id);

                        db.Estudante.Remove(aluno);
                        db.SaveChanges();
                    };
                }
            }
        }
    }
}
