﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WindowsFormsApp1.Mysql
{
    public partial class produtoContext : DbContext
    {
        public produtoContext()
        {
        }

        public produtoContext(DbContextOptions<produtoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<produto> Produto { get; set; }
        public virtual DbSet<Fornecedor> Fornecedor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=localhost;User Id=root;Password=;Database=alunos");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<produto>(entity =>
            {
                entity.ToTable("produto");

                entity.HasIndex(e => e.IdfornecedorNavigation)
                    .HasName("FK_IdFornecedor_Produto");

                entity.Property(e => e.descricao)
                    .HasColumnName("id")
                    .HasColumnType("string");

                entity.Property(e => e.unidade)
                    .HasColumnName("unidade")
                    .HasColumnType("int");

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasColumnType("blob");

                entity.Property(e => e.marca)
                    .HasColumnName("marca")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Garantia)
                    .HasColumnName("garantia")
                    .HasColumnType("string");

                entity.Property(e => e.modelo)
                    .IsRequired()
                    .HasColumnName("modelo")
                    .HasColumnType("string");

                entity.HasOne(d => d.IdfornecedorNavigation)
                    .WithMany(p => p.Produto)
                    .HasForeignKey(d => d.IdfornecedorNavigation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IdSala_Estudante");
            });

            modelBuilder.Entity<Fornecedor>(entity =>
            {
                entity.ToTable("sala");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(50)");
            });
        }
    }
}
