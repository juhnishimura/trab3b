﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApp1.Mysql
{
    public partial class produto
    {
        public int cod_produto { get; set; }
        public string descricao { get; set; }
        public int unidade { get; set; }
        public byte[] Foto { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string Garantia { get; set; }
        public string fornecedor { get; set; }
        public Fornecedor IdfornecedorNavigation { get; set; }
    }
}
