﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApp1.Mysql
{
    public partial class Fornecedor
    {
        public Fornecedor()
        {
            Produto = new HashSet<produto>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }

        public ICollection<produto> Produto { get; set; }
    }
}
